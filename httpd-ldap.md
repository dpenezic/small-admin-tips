## Debug httpd-ldap connection

To debug mod_ldap (and mod_authz_ldap) you need to have both services (Apache and LDAP) in debug mode.

For Apache use configuration parameter in `<VirtualHost>`  sequence :

```
LogLevel debug
```

More info available on follow URL [https://www.loggly.com/ultimate-guide/apache-logging-basics/](https://www.loggly.com/ultimate-guide/apache-logging-basics/)

For LDAP service (if you use OpenLdap) stop standard service and start OpenLdap in foreground debug mode with (example):

```
slapd -d 128
```

More info available on follow URL [https://www.openldap.org/doc/admin24/runningslapd.html](https://www.openldap.org/doc/admin24/runningslapd.html)

## Some tips
* Centos and SULinux
    Enabled communication between httpd and LDAP executing follow command

    ```
    # getsebool -a | grep ldap
    authlogin_nsswitch_use_ldap --> off
    httpd_can_connect_ldap --> off

    # setsebool httpd_can_connect_ldap 1

    # getsebool -a | grep ldap
    authlogin_nsswitch_use_ldap --> off
    httpd_can_connect_ldap --> on
    ```
* `require valid-user` use for `BasicAuth`
    - Be aware that `BasicAuth` username will be compare with LDAP attribute you specific in atributte list when set LDAP connection (**mail** in follow example) :

        ```
        AuthLDAPURL "ldap://127.0.0.1:389/dc=pero,dc=hr?mail" NONE
        ```
    
        Additional in most example on Internet word NONE is inside "" which is wrong !

    - To satisfied Apache parser you will need to add follow line if you use only LDAP authentication :

        ```
        AuthUserFile /dev/null
        ``` 